#!/usr/bin/python3
import pyttsx3
import os,sys

if __name__=='__main__':
    engine=pyttsx3.init()
    engine.setProperty('voice', 'zh')
    skip = 0
    if len(sys.argv)==3:
        skip = int(sys.argv[2])

    n = 0
    with open(sys.argv[1],encoding='utf-8') as f:
        for text in f:
            n+=1
            if n<=skip:
                continue
            try:
                engine.say(text)
                engine.runAndWait()
            except:
                print( print("Unexpected error:", sys.exc_info()[0]) )
                break
            print( str(n) )
            

