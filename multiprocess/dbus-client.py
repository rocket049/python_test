from pydbus import SessionBus
from gi.repository import GLib
bus = SessionBus()
obj = bus.get('test.server.hello')['test.server.hello']

def hello_handler(usr):
    print('hello %s'%usr)

from threading import Thread
class sub_proc(Thread):
    def __init__(self,obj):
        Thread.__init__(self)
        self.o = obj
    def run(self):
        loop = GLib.MainLoop()
        # 绑定 hello 信号处理函数
        self.o.hello.connect(hello_handler)
        loop.run()
            
t=sub_proc(obj)
t.start()
for i in range(5):
    # 循环调用 set 方法
    s=input('>')
    obj.set(s)
t.join()
