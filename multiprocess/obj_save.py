import json

def save_obj(obj,path):
    fp = open(path,'wb')
    j_obj = json.dumps(obj).encode('utf-8')
    size = len(j_obj)
    head = hex(size)
    if len(head)<8:
        head = head.ljust(8).encode('utf-8')
    fp.write( head + j_obj )
    fp.close()
    return 0

def load_obj(path):
    fp = open(path,'rb')
    head = fp.read(8)
    size = int(head.decode('utf8'),16)
    body = fp.read(size)
    fp.close()
    obj = json.loads( body.decode('utf-8') )
    return obj
