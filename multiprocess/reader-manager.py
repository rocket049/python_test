from multiprocessing.managers import BaseManager
from _thread import start_new_thread

class QueueManager(BaseManager): pass
QueueManager.register('get_queue')
m = QueueManager(address=('localhost', 50000), authkey='abracadabra'.encode('utf-8'))
m.connect()
queue = m.get_queue()

def read_queue(q):
    while True:
        str1 = q.get()
        print( str1 )

def read_stdin():
    while True:
        str1 = input('>')
        if str1 == 'q':
            break
        print('input: '+str1)

id1 = start_new_thread(read_queue,(queue,) )
read_stdin()
