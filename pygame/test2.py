import pygame
from pygame.locals import *
from sys import exit
from threading import Thread,Lock
import time,math

#273 上
#274 下
#275 左
#276 右
#32  空格
#27  ESC

LEFT_DOWN=False
RIGHT_DOWN=False
LOCK1 = Lock()
is_alive = True
def update_screen():
    global is_alive,LOCK1
    clock1 = pygame.time.Clock()
    cycle = 0
    ship1_event = pygame.event.Event(USEREVENT, message="h")
    while is_alive:
        try:
            LOCK1.acquire()
            pygame.display.update()
            if cycle%25==0:
                pygame.event.post(ship1_event)
            cycle += 1
            LOCK1.release()
        except:
            pass
        clock1.tick(60)
        
background_image_filename = 'pics/bg2.png'
ship_image_filename = 'pics/ship1.png'
hole_img_filename = 'pics/hole1.png'
 
pygame.init()
SCREEN_SIZE = (800, 600)
screen = pygame.display.set_mode(SCREEN_SIZE, 0, 32)
t= Thread(target=update_screen)
t.start()

background = pygame.image.load(background_image_filename).convert()
ship1 = pygame.image.load(ship_image_filename)
hole1 = pygame.image.load(hole_img_filename)

font = pygame.font.Font("hei1.ttf", 18);
font_height = font.get_linesize()

pygame.event.set_blocked(None)
pygame.event.set_allowed( [MOUSEBUTTONDOWN,KEYDOWN,KEYUP,QUIT,ACTIVEEVENT,USEREVENT] )

Fullscreen = False
message = ''
x = 358
y = 273
target = (x+50,y+15)
gun_aim = (386,291)
start_time = time.time()
dy=0
while True:
    event = pygame.event.wait()
    d_time = time.time()-start_time
    d_y = int(math.sin( math.pi*d_time/6.0)*30)
    if event.type == QUIT:
        is_alive = False
        break
        
    LOCK1.acquire()
    if event.type == KEYDOWN:
        if event.key == K_f and event.mod==256:
            Fullscreen = not Fullscreen
            if Fullscreen:
                screen = pygame.display.set_mode(SCREEN_SIZE, FULLSCREEN, 32)
            else:
                screen = pygame.display.set_mode(SCREEN_SIZE, 0, 32)
        elif event.key == 273:
            dy += 1
            message = '你按下了：上 (%s,%s)'%(x,y)
        elif event.key == 274:
            dy -= 1
            message = '你按下了：下 (%s,%s)'%(x,y)
        elif event.key == 275:
            RIGHT_DOWN = True
            message = '你按下了：右 (%s,%s)'%(x,y)
        elif event.key == 276:
            LEFT_DOWN = True
            message = '你按下了：左 (%s,%s)'%(x,y)
        elif event.key == 32:
            message = '你按下了：空格 target(%s,%s) gun(%s,%s)' % ( target[0],target[1],gun_aim[0],gun_aim[1] )
        elif event.key == 27:
            message = '你按下了：ESC键'
    elif event.type == KEYUP:
        if event.key == 275:
            RIGHT_DOWN = False
        elif event.key == 276:
            LEFT_DOWN = False
    elif event.type == USEREVENT:
        if event.message=='h':
            x -= 1
<<<<<<< HEAD
    elif event.type == MOUSEBUTTONDOWN:
        message = str(event)
            
=======
            if LEFT_DOWN:
                x+=3
            elif RIGHT_DOWN:
                x-=2
    elif event.type==MOUSEBUTTONDOWN:
        message = str(event)
    target = (x+50,y+d_y+15)
>>>>>>> a944447f9ba6e6c1ba7552bd114dee5442e46937
    screen.fill((255, 255, 255))
    screen.blit(background, (0,d_y))
    screen.blit(ship1,(x,y+d_y))
    screen.blit(hole1,(0,0))
    pygame.draw.circle( screen,(255,0,0),gun_aim,3 )
    screen.blit( font.render(message, False, (0, 255, 0)), (10, font_height) )
    
    LOCK1.release()
