#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  myaddr.py
#  
#  Copyright 2017 fhz <fhz@fhz-Pad>
#  
#  本程序用二维码的形式显示本机 8080 端口的 http 访问地址
#  使用 qrcode 生成二维码，用 matplotlib 显示图片
#  

import matplotlib.pyplot as plt
def main(args):
    import socket,os,re
    import qrcode
    #print(socket.gethostname())
    out = os.popen("ifconfig | grep 'inet .*broadcast'").read()
    m = re.findall('inet [\d\.]+',out)
    #filename = ''
    for s in m:
        myip = s.split()[1]
        print( 'http://'+ myip +':8080/' )
        img = qrcode.make( 'http://'+ myip +':8080/' )
        plt.figure( 'http://'+ myip +':8080/' )
        plt.axis('off')
        plt.imshow(img)
        plt.show()
        #filename = '%s.png' %myip
        #img.save(filename)
        del img
    #os.system('xdg-open %s' %filename)
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
