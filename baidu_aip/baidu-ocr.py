from aip import AipOcr

""" 你的 APPID AK SK """
APP_ID = 'APP_ID'
API_KEY = 'API_KEY'
SECRET_KEY = 'SECRET_KEY'

def get_middle(res):
	max1 = res[0]['location']['left']
	min1 = max1
	for words in res:
		left = words['location']['left']
		if left>max1:
			max1 = left
		if left<min1:
			min1 = left
	return (max1+min1)/2

""" 读取图片 """
def get_file_content(filePath):
	try:
		with open(filePath, 'rb') as fp:
			return fp.read()
	except:
		return b''

if __name__ == '__main__':
	client = AipOcr(APP_ID, API_KEY, SECRET_KEY)
	
	for i in range(100):
		image = get_file_content(str(i)+'.png')
		if image==b'':
			break
		""" 调用通用文字识别（高精度版带位置信息） """
		res = client.accurate(image)
		mid = get_middle(res['words_result'])
		for words in res['words_result']:
			str1 = words['words']
			if words['location']['left']>mid:
				sys.stdout.write('\n    '+str1)
			else:
				sys.stdout.write(str1)
		print('\n')


