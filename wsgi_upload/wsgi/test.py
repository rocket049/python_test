#!/usr/bin/env python3

from cgi import escape
import tempfile
import sys
apps = {}

sys.path.append('/home/fuhz/script/wsgi')

def application(environ, start_response):
    try:
        func = apps[escape( environ['mod_wsgi.path_info'] )]
    except:
        try:
            func = apps[escape( environ['PATH_INFO'] )]
        except:
            pass
    try:
        response_body = func(environ, start_response)
    except:
        response_body = test(environ, start_response)
    return response_body
    
def test(environ, start_response):
    response_body = ['%s: %s' % (key, value)  for key, value in sorted(environ.items())]  
    response_body = '\n'.join(response_body)
    status = '200 OK'
    #status = '500 InternalServerError'
    response_headers = [('Content-Type', 'text/plain'),  ('charset','UTF-8'),('Content-Length', str(len(response_body.encode('utf-8'))))] 
    start_response(status, response_headers)
    return [response_body.encode('utf-8')]
apps['/test'] = test

import upload
apps['/upload'] = upload.app

import test2
apps['/test2'] = test2.app
