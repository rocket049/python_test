#!/usr/bin/env python3
# 批量修改各种类型图片大小 ,用法(缩小一半)： ./img_scale.py 0.5 *.png 
from PIL import Image,ImageDraw
import sys,os

def img_scale( imgpath , scale ):
    img = Image.open(imgpath)
    scale_size = ( int(img.width*scale), int(img.height*scale) )
    newimg = img.resize(scale_size,resample=Image.NEAREST)
    try:
        os.mkdir('scaled')
    except:
        pass
    pathpair = os.path.split(imgpath)
    newpath = pathpair[0]+'./scaled/'+pathpair[1]
    print(newpath)
    newimg.save(newpath)
    
if __name__=='__main__':
    try:
        if len(sys.argv)<3:
            raise Exception
        for i in range(2,len(sys.argv)):
            img_scale( sys.argv[i],float(sys.argv[1]) )
    except:
        print('批量缩放各种图片，例如：批量缩小一半尺寸: img_scale.py 0.5 *.jpg')
