from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time,sys
from lxml import etree

def parse_html(html):
    res = []
    dom = etree.HTML( html )
    date1 = dom.xpath('//thead/tr[1]/th[5]')
    #date1 = tr1.xpath('th[5]')
    res.append('# '+date1[0].text+'\n')
    funds = dom.xpath('//tbody/tr')
    #[@class="odd"]
    print('total:',len(funds))
    for f in funds:
        link_hm = f.find('td[3]/a')
        jjhm = link_hm.text
        href = link_hm.get('href')
        name = f.find('td[4]/a').text
        jz = f.find('td[5]').text
        ljjz = f.find('td[6]').text
        try:
            res.append('- %s %s https://www.howbuy.com%s %s %s\n'%(jjhm,name,href,eval(jz),eval(ljjz)))
        except:
            pass
    return res

def save_data( html ):
    ret = parse_html(html)
    print('used:',len(ret))
    fp = open('output/howbuy.md','w')
    for item in ret:
        fp.write(item)
    fp.close()

def test(filename):
    data = ''
    fp = open(filename,'r')
    for dat in fp:
        data = data + dat
    fp.close()
    save_data( data )
    
#test('howbuy.htm')
#sys.exit(0)

from pyvirtualdisplay import Display
display = Display(visible=0,size=(800,600))
display.start()
try:
    url1='https://www.howbuy.com/board/'
    #content: div id="tableMain"
    #div class="result_list"
    #footer: div class="footer ftArial"
    driver = webdriver.Chrome(r'/usr/lib/chromium-browser/chromedriver')
    driver.get( url1 )
    time.sleep(2)
    posY = 0
    while True:
        #element  = WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.XPATH,'//div[@class="footer ftArial"]')))
        element = driver.find_element_by_xpath('//div[@class="footer ftArial"]')
        pos1 = element.location
        if pos1['y'] == posY:
            break
        posY = pos1['y']
        print( posY )
        pos2 = element.location_once_scrolled_into_view
        time.sleep(0.5)
    #element = WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.ID, "tableMain")))
    element = driver.find_element_by_id("tableMain")
    save_data( element.get_attribute('innerHTML') )
    print('data saved')
    driver.quit()
except Exception as e:
    print(e)
display.stop()
