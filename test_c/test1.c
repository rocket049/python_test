#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

extern int errno;

typedef struct member{
    unsigned short id;
    char name[20];
    unsigned short sex : 1;
}  memberx;

int file_try(char *filename);

int main(int argc,char **argv){
    float a=1.3;
    int x=1;
    long y=1;
    long double z=0.1;
    double b=0.4;
    memberx *m=NULL;
    printf("length of 'int' : %u \n 'long' : %u \n 'long double' : %u\
     \n 'float' : %u \n 'double' : %u \n",\
        sizeof(x),sizeof(y),sizeof(z),sizeof(a),sizeof(b));
        
    m = (memberx *)malloc(sizeof(memberx));
    for(int i=0;i<argc;i++){
        m->id=i+1;
        strcpy(m->name,argv[i]);
        m->sex = i%2;
        printf("m1 id: %d ,name: %s ,sex: %d\n",m->id,m->name,m->sex);
    }
    free(m);
    for(int i=0;i<argc;i++){
        file_try(argv[i]);
    }
    exit(0);
}

int file_try(char *filename){
   FILE * pf;
   int errnum;
   printf("try file : %s\n",filename);
   pf = fopen (filename, "rb");
   if (pf == NULL)
   {
      errnum = errno;
      fprintf(stderr, "Value of errno: %d\n", errno);
      perror("Error printed by perror");
      fprintf(stderr, "Error opening file: %s\n", strerror( errnum ));
   }
   else
   {
      fclose (pf);
      printf("success : %s\n",filename);
   }
   return 0;
}
