#!/usr/bin/env python3

# CGI处理模块
import cgi, cgitb

# 创建 FieldStorage 的实例化
form = cgi.FieldStorage() 

# 获取数据
site_name = form.getvalue('name1')
site_text = form.getvalue('text1')
fp1 = open( '/var/www/upload/'+site_name , mode='w' )
fp1.write(site_text)
fp1.close()

print("Content-type: text/html;encoding=UTF-8\n\n")

print ("<html>")
print ("<head>")
print ('<meta charset="utf-8">')
print ("<title>内容回馈</title>")
print ("</head>")
print ("<body>")
print ("<h2>"+site_name+"</h2>" )
print("<p>"+site_text+"</p>" )
print ("</body>")
print ("</html>")

