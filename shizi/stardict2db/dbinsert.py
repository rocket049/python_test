# -*- coding: utf-8 -*-
"""
Copyright 2008 Serge Matveenko

This file is part of PyStarDict.

PyStarDict is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyStarDict is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyStarDict.  If not, see <http://www.gnu.org/licenses/>.

@author: Serge Matveenko <s@matveenko.ru>
"""
import datetime
import os
import sys
import re, sqlite3
from pystardict import Dictionary

dicts_dir = os.getcwd()
dict1 = Dictionary(os.path.join(dicts_dir, 'stardict-xhzd-2.4.2', 'xhzd'))

def db_insert( w, m ):
    db1 = sqlite3.connect( os.path.join(os.getcwd(), 'mydict.db') )
    db1.text_factory = str
    cu1 = db1.cursor()
    print m
    cu1.execute( "insert into dict(word, mean) values( ? , ? )", (w, m))
    cu1.close()
    db1.commit()
    db1.close()

if __name__ == '__main__':
    if len(sys.argv[1])>0:
        db_insert(sys.argv[1],  dict1.dict[sys.argv[1]])
