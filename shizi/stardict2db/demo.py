# -*- coding: utf-8 -*-
"""
Copyright 2008 Serge Matveenko

This file is part of PyStarDict.

PyStarDict is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyStarDict is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyStarDict.  If not, see <http://www.gnu.org/licenses/>.

@author: Serge Matveenko <s@matveenko.ru>
"""
import datetime
import os
import sys
import re
from pystardict import Dictionary

def demo(keys1):
    
    milestone1 = datetime.datetime.today()
    
    dicts_dir = os.getcwd()
    dict1 = Dictionary(os.path.join(dicts_dir, 'stardict-xhzd-2.4.2',
        'xhzd'))
       
    milestone2 = datetime.datetime.today()
    print ('1 dicts load:', milestone2-milestone1)

    for name1 in keys1:
        print( name1+ ' : ')
        try:
            print( str(dict1.idx[name1]) )
        except:
            pass
    
    milestone3 = datetime.datetime.today()
    print( '2 cords getters:',  milestone3-milestone2)
    
    for name1 in keys1:
        print( name1+' : ' )
        try:
            print( re.sub('<br>','\n',dict1.dict[name1]) )
        except:
            pass
    
    milestone4 = datetime.datetime.today()
    print( '2 direct data getters (w\'out cache):', milestone4-milestone3)
    
#    for name1 in keys1:
#        print name1, ' : ',
#        try:
#            print re.replace(dict1[name1], '<br>', '\n')
#        except:
#            pass
#
#    milestone5 = datetime.datetime.today()
#    print '2 high level data getters (not cached):', milestone5-milestone4
    
#    for name1 in keys1:
#        print name1, ' : '
#        try:
#            print dict1[name1]
#        except:
#            pass
#    
#    milestone6 = datetime.datetime.today()
#    print '2 high level data getters (cached):', milestone6-milestone5

dicts_dir = os.getcwd()
dict1 = Dictionary(os.path.join(dicts_dir, 'stardict-xhzd-2.4.2', 'xhzd'))


if __name__ == '__main__':
    keys=['卖']
#    keys = ['龙', '人', '蛇']
#    demo( keys )
    for w1 in keys:
        print re.sub('<br>','\n',dict1.dict[w1])
