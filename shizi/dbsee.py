import sys, os
import sqlite3

def get_mean( word ):
    db1 = sqlite3.connect( os.path.join(os.getcwd(), 'mydict.db') )
    cu1 = db1.cursor()
    sql1 = "select mean from dict where word='%s'"%(word, )
    cu1.execute(sql1)
    mean=cu1.fetchone()
    cu1.close()
    db1.close()
    return mean[0]
    
def show_10( ):
    db1 = sqlite3.connect( os.path.join(os.getcwd(), 'mydict.db') )
    cu1 = db1.cursor()
    sql1 = "select word from dict limit 10"
    cu1.execute(sql1)
    for item in cu1:
        print(item[0])
    cu1.close()
    db1.close()
    
if __name__ == '__main__':
    try:
        if len(sys.argv[1])>0:
            print( get_mean(sys.argv[1]) )
    except:
        show_10()
