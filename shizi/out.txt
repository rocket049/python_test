tag:html:0:

	tag:head:1:
    
		tag:meta:2:
    
			tag:meta:3:
    
				tag:meta:4:
    
					tag:link:5:
    
					tag:meta:5:
    
					tag:meta:5:
    
					tag:title:5:百度词典_龙
    
					tag:script:5:
var _hmt = _hmt || [];
var _ziciURL = "//hm.baidu.com/hm.js?aa7a77b165b2c996a2c4b0493fbdbc2b";
var _dictURL = "//hm.baidu.com/hm.js?304463b8a93092da95f0ed0515fb47be";

(function() {
  var hm = document.createElement("script");
  hm.src = _dictURL;
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
        
					tag:link:5:
    
					tag:link:5:
    
				tag:body:4:


					tag:div:5:

						tag:script:6:
var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F304463b8a93092da95f0ed0515fb47be' type='text/javascript'%3E%3C/script%3E"));

window.__start_time = +(new Date());


    
					tag:div:5:
        
						tag:div:6:
        
							tag:div:7:
    
								tag:div:8:
									tag:a:9:
    
								tag:div:8:
        
									tag:form:9:
            
										tag:span:10:
                
											tag:input:11:
                
												tag:input:12:
            
            
												tag:span:12:
                
													tag:input:13:
            
        
    
        
										tag:div:10:
    
    
											tag:ul:11:
    
												tag:li:12:
        
													tag:a:13:登录
    
    
												tag:li:12:|
    
												tag:li:12:
													tag:a:13:手机版
    
												tag:li:12:|
    
												tag:li:12:
        
													tag:a:13:百度首页
    
    
        
    
    
    
								tag:div:8:
    
									tag:div:9:
        
										tag:div:10:
            
											tag:div:11:用 
												tag:a:12:百度汉语 查看更全面更权威的汉语知识，在百度汉语中查看 “
												tag:a:12:龙”
												tag:img:12:
            
												tag:a:12:
													tag:img:13:
        
        
											tag:div:11:
        
											tag:div:11:
            
												tag:div:12:
    


												tag:div:12:
    
    
													tag:div:13:
        
														tag:div:14:
                        
															tag:img:15:
                    
        
														tag:div:14:
            
															tag:ul:15:
                                    
																tag:li:16:
																	tag:label:17:拼 音
																	tag:span:17:
																		tag:div:18:
																			tag:span:19:
																				tag:b:20:lóng
																				tag:a:20: 
                                    
																tag:li:16:
																	tag:label:17:部 首
																	tag:span:17:龙
                                    
																tag:li:16:
																	tag:label:17:繁 体
																	tag:span:17:龍
                                    
																tag:li:16:wb
																	tag:label:17:五 笔
																	tag:span:17:DXV
                                    
																tag:li:16:
																	tag:label:17:笔 顺
																	tag:span:17:一ノフノ丶
                            
        
                
														tag:a:14:生词本
        
														tag:script:14:window.basicInfo = {"name":"\u9f99","pinyin":"l\u00f3ng","definition":"l\u00f3ng#long2#1.\u4f20\u8bf4\u4e2d\u7684\u4e00\u79cd\u957f\u5f62\u3001\u6709\u9cde\u3001\u6709\u89d2\u7684\u795e\u5f02\u52a8\u7269\uff0c\u80fd\u8d70\uff0c\u80fd\u98de\uff0c\u80fd\u6e38\u6cf3\uff0c\u80fd\u5174\u4e91\u4f5c\u96e8\uff1a\uff5e\u821f\u3002\uff5e\u706f\u3002\uff5e\u5bab\u3002\uff5e\u9a79\uff08\u9a8f\u9a6c\uff0c\u55bb\u624d\u534e\u51fa\u4f17\u7684\u5c11\u5e74\uff09\u3002\u753b\uff5e\u70b9\u775b\u3002\uff5e\u87e0\u864e\u8e1e\u3002\\n2.\u53e4\u751f\u7269\u5b66\u4e2d\u6307\u4e00\u4e9b\u5de8\u5927\u7684\u6709\u56db\u80a2\u6709\u5c3e\u6216\u517c\u6709\u7ffc\u7684\u722c\u866b\uff1a\u6050\uff5e\u3002\\n3.\u5c01\u5efa\u65f6\u4ee3\u7528\u4f5c\u7687\u5e1d\u7684\u8c61\u5f81\uff0c\u6216\u79f0\u5173\u4e8e\u7687\u5e1d\u7684\u4e1c\u897f\uff1a\uff5e\u989c\u3002\uff5e\u4f53\u3002\uff5e\u888d\u3002\\n4.\u59d3\u3002"};
    

    
    

													tag:div:13:
    
														tag:h1:14:
															tag:b:15:基本释义
															tag:span:15:
                
															tag:a:15:详细解释
        
															tag:span:15:
            
    
														tag:div:14:
    
															tag:dl:15:
																tag:dd:16:
																	tag:ol:17:
																		tag:li:18:
																			tag:p:19:传说中的一种长形、有鳞、有角的神异动物，能走，能飞，能游泳，能兴云作雨：～舟。～灯。～宫。～驹（骏马，喻才华出众的少年）。画～点睛。～蟠虎踞。
																		tag:li:18:
																			tag:p:19:古生物学中指一些巨大的有四肢有尾或兼有翼的爬虫：恐～。
																		tag:li:18:
																			tag:p:19:封建时代用作皇帝的象征，或称关于皇帝的东西：～颜。～体。～袍。
																		tag:li:18:
																			tag:p:19:姓。
    
    
														tag:div:14:
        
															tag:a:15:
																tag:b:16:︾查看更多
        
															tag:a:15:
																tag:b:16:︽收起更多
    




													tag:div:13:
    
														tag:h1:14:
                
															tag:a:15:基本释义
                
															tag:span:15:
        
															tag:b:15:详细解释
        
														tag:span:14:
    
														tag:div:14:
    
															tag:dl:15:
																tag:dd:16:
																	tag:p:17:
																		tag:strong:18:〈名〉
																	tag:ol:17:
																		tag:li:18:
																			tag:p:19: (象形。甲骨文,象龙形。本义:古代传说中一种有鳞有须能兴云作雨的神异动物)
																		tag:li:18:
																			tag:p:19: 同本义 [dragon]
																			tag:p:19:龍,鳞虫之长。能幽能明,能细能巨,能短能长。春分而登天,秋分而潜渊。——《说文》。徐铉注:“象宛转飞动之貌。” 饶炯注:“龙之为物,变化无端,说解因着其灵异如此,以能升天,神其物,而命之曰灵。”
																			tag:p:19:飞龙在天。——《易·乾》
																			tag:p:19:麟、凤、龟、龙,谓之四灵。——《礼记·礼运》
																			tag:p:19:甲虫三百有六十,而龙为之长。——《孔子家语·执辔》
																			tag:p:19:叶公子高好龙。—— 刘向《新序·叶公好龙》
																		tag:li:18:
																			tag:p:19: 又如:龙工(像龙一样熟悉水性之功。也作“龙功”);龙公(指龙王,龙神);龙渊(龙所栖止的深渊);龙章(龙形的图案);龙蛇(龙和蛇);龙文(龙状花纹);龙伯(传说中的水神);龙沼(龙池);龙旌凤翣(有龙凤图案的旌旗和大掌扇);龙门(本是跨在黄河上游的山名。神话传说,鱼类跳过龙门,就可以变成神龙。后借指乡试考场的二门或三门为龙门)
																		tag:li:18:
																			tag:p:19: 封建时代用龙作为皇帝的象征 [imperial]
																			tag:p:19:祖龙死,谓始皇也。祖,人之本;龙,人君之象也。——《论衡·纪妖》
																		tag:li:18:
																			tag:p:19: 又如:龙升(龙的升子。比喻天子即位);龙辇(天子的乘车);龙颜(额头隆起似龙。后世以喻皇帝的容貌。也用以指皇帝);龙腾(比喻帝王的兴起);龙鳞(喻指皇帝或皇帝的威严);龙驭(皇帝车驾。代指皇帝);龙兴(帝业兴旺;创立帝业);龙节(皇帝所授与的符节);龙衮(帝王的礼服);龙下蛋(比喻不可能);龙穴(最适宜埋棺材的好地方);龙御(皇帝的车驾);龙凤(旧时用以形容帝王的相貌)
																		tag:li:18:
																			tag:p:19: 喻不凡之士,豪杰之士 [outstanding person]
																			tag:p:19:丞尉等并衣冠之龟龙,人物之标准。——唐· 李白《化城寺大钟铭序》
																		tag:li:18:
																			tag:p:19: 又如:龙虎(比喻杰出的人物);龙蛇(比喻非常的人);龙逸(如龙隐逸。比喻贤人隐居于野);龙凤(比喻才能优异的人);龙驹(比喻俊才);龙鹏(龙和鹏。比喻贤俊豪杰);龙翰凤翼(比喻君子、贤者);龙蟠凤逸(比喻才能卓越超群而未为世用的人)
																		tag:li:18:
																			tag:p:19: 喻骏马 [fine horse]
																			tag:p:19:马八尺以上为龙。——《周礼·夏官》
																		tag:li:18:
																			tag:p:19: 又如:龙子(良马名);龙文(骏马名);龙姿(骏马的形貌);龙孙(良马名);龙驹(骏马);龙骥(骏马);龙媒(指天马、骏马)
																		tag:li:18:
																			tag:p:19: 喻文章,书法的雄健华丽 [vigorous]。如:龙蛇(龙蛇走。形容笔势如龙蛇,蜿蜒盘曲);龙文(比喻文章的雄健);龙虎(比喻文章的雄健);龙章(比喻文章富丽华美,如龙的文采);龙藻(比喻华丽的辞藻);龙骧豹变(比喻书法气势雄放,变化无穷)
																		tag:li:18:
																			tag:p:19: 比喻性格亢直 [upright]。如:龙亢(个性刚直不屈);龙性(指性格倔强,难以驯服)
																		tag:li:18:
																			tag:p:19: 〈方〉∶[瓦圈] 歪扭不圆 [not round]。如:自行车前轱辘龙了
																		tag:li:18:
																			tag:p:19: 姓
    
    
														tag:div:14:
        
															tag:a:15:
																tag:b:16:︾查看更多
        
															tag:a:15:
																tag:b:16:︽收起更多
    


    
    
    
													tag:div:13:
    
													tag:div:13:
        
														tag:h1:14:
															tag:b:15:其他信息
        
														tag:div:14:
                            
															tag:div:15:
                    
																tag:label:16:五 笔
																tag:span:16:DXV
                
                            
															tag:div:15:
                    
																tag:label:16:四 角
																tag:span:16:43014
                
                            
															tag:div:15:
                    
																tag:label:16:仓 颉
																tag:span:16:IKP
                
                            
															tag:div:15:
                    
																tag:label:16:五行
																tag:span:16:火
                
                            
															tag:div:15:
                    
																tag:label:16:笔画数
																tag:span:16:5
                
                            
															tag:div:15:
                    
																tag:label:16:郑码
																tag:span:16:GM
                
                            
															tag:div:15:
                    
																tag:label:16:字结构
																tag:span:16:单一结构
                
                            
															tag:div:15:
                    
																tag:label:16:部件拆解
																tag:span:16:龙
                
                            
															tag:div:15:
                    
																tag:label:16:注音
																tag:span:16:ㄌㄨㄥˊ
                
                            
															tag:div:15:
                    
																tag:label:16:异体字
																tag:span:16:龒、龍、竜
                
                            
															tag:div:15:
                    
																tag:label:16:统一码
																tag:span:16:9F99
                
                            
															tag:div:15:
                    
																tag:label:16:
																tag:span:16:
                
                    
    
    
    
        
													tag:div:13:
        
														tag:h1:14:
															tag:b:15:相关组词
        
														tag:div:14:
                        
															tag:a:15:龙化
                        
															tag:a:15:火龙
                        
															tag:a:15:懒龙
                        
															tag:a:15:龙东
                        
															tag:a:15:龙腰
                        
															tag:a:15:怀龙
                        
															tag:a:15:龙湖
                        
															tag:a:15:酒龙
                        
															tag:a:15:龙夔
                        
															tag:a:15:龙蜕
                        
															tag:a:15:龙关
                        
															tag:a:15:龙麝
                        
															tag:a:15:龙鉢
                        
															tag:a:15:龙恩
                        
															tag:a:15:土龙
                        
															tag:a:15:龙牵
                        
															tag:a:15:龙纱
                        
															tag:a:15:小龙
                        
															tag:a:15:虬龙
                        
															tag:a:15:龙修
                    
    
    
    
    

            
												tag:div:12:
            
													tag:div:13:
                                
														tag:div:14:
                
															tag:div:15:
    
																tag:h1:16:相关字
    
																tag:ul:16:
            
																	tag:li:17:
        
																		tag:a:18:
            
																			tag:img:19:
        
        
																			tag:a:19:东
    
                
																		tag:li:18:
        
																			tag:a:19:
            
																				tag:img:20:
        
        
																				tag:a:20:刘
    
                
																			tag:li:19:
        
																				tag:a:20:
            
																					tag:img:21:
        
        
																					tag:a:21:到
    
                
																				tag:li:20:
        
																					tag:a:21:
            
																						tag:img:22:
        
        
																						tag:a:22:原
    
                
																					tag:li:21:
        
																						tag:a:22:
            
																							tag:img:23:
        
        
																							tag:a:23:及
    
                
																						tag:li:22:
        
																							tag:a:23:
            
																								tag:img:24:
        
        
																								tag:a:24:回
    
            


																					tag:div:21:
    
																						tag:h1:22:热搜字
    
																						tag:ul:22:
            
																							tag:li:23:
        
																								tag:a:24:
            
																									tag:img:25:
        
        
																									tag:a:25:乖
    
                
																								tag:li:24:
        
																									tag:a:25:
            
																										tag:img:26:
        
        
																										tag:a:26:乜
    
                
																									tag:li:25:
        
																										tag:a:26:
            
																											tag:img:27:
        
        
																											tag:a:27:吮
    
                
																										tag:li:26:
        
																											tag:a:27:
            
																												tag:img:28:
        
        
																												tag:a:28:好
    
                
																											tag:li:27:
        
																												tag:a:28:
            
																													tag:img:29:
        
        
																													tag:a:29:应
    
                
																												tag:li:28:
        
																													tag:a:29:
            
																														tag:img:30:
        
        
																														tag:a:30:手
    
            

                
            
        
    
    
    
    
    



																				tag:div:20:


																				tag:p:20:©2017 Baidu
        
																					tag:a:21:使用百度前必读 
        
																					tag:a:21:百度首页
        
																					tag:a:21:站内搜索
        
																					tag:a:21:问题反馈
        
																					tag:a:21:关注微博
        
																					tag:span:21:用户QQ群：484758177



																				tag:div:20:
																					tag:audio:21:


																				tag:script:20:

																				tag:script:20:


																				tag:script:20:

require.config(
    {
        baseUrl: '/asset/asset',
        urlArgs: 'v=0.2.1'
    }
);
require(['main']);

$("#kw")[0].focus();
window.__finish_time = + (new Date());
window.__used_time = __finish_time - window.__start_time;






