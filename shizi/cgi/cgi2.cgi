#!/usr/bin/env python3

import os

import sys

print("Content-type: text/html;encoding=UTF-8\n\n")
print ('<html>')
print ('<head>')
print ('<meta charset="utf-8" />')
print ('<title>Hello Word</title>')
print ('</head>')
print ('<body>')
print ('<h2>system default: '+sys.getdefaultencoding()+'</h2>')
print ('<h2>stdout default: '+sys.stdout.encoding+'</h2>')
for e1 in os.environ:
	print(e1+' : '+os.environ[e1]+'<br/>')
print ('</body>')
print ('</html>')
