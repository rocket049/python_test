#!/usr/bin/env python3

import sys
sys.stdout = open(sys.stdout.fileno(), mode='w', encoding='utf8', buffering=1)

print ("Content-type:text/html;encoding=UTF-8\n\n")
print ('<html>')
print ('<head>')
print ('<meta charset="utf-8" />')
print ('<title>Hello Word - 我的第一个 CGI 程序！</title>')
print ('</head>')
print ('<body>')
print ('<h2>Hello Word! 我是来自菜鸟教程的第一CGI程序</h2>')
print ('<h2>coding:'+sys.getdefaultencoding()+'</h2>')
print ('</body>')
print ('</html>')
