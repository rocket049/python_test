#!/usr/bin/python3

# CGI处理模块
import cgi, cgitb, postgresql

# 创建 FieldStorage 的实例化
form = cgi.FieldStorage() 

# 获取数据
site_name = form.getvalue('name1')
site_text = form.getvalue('text1')
sql1 = "insert into upload_tbl1( name,content,upload_date) values('%s','%s', LOCALTIMESTAMP(0) )"%(site_name, site_text)
db1 = postgresql.open('pq://user1:user049@localhost:5432/udb1')
ps1=db1.prepare(sql1)
ret1 = ps1()
db1.close()
print("Content-type: text/html;encoding=UTF-8\n\n")
print ("<html>")
print ("<head>")
print ('<meta charset="utf-8" />')
print ("<title>内容回馈</title>")
print ("</head>")
print ("<body>")
print ("<h2>"+site_name+"</h2><br/>" )
print("<p>"+site_text+"</p>" )
print("<p>result: ")
for r1 in ret1:
	print(str(r1))
print ("</p>\n</body>")
print ("</html>")
